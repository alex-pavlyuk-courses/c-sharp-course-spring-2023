namespace ThreadingSamples;

public class MyList<T>
{
    private const int DefaultCapacity = 4;

    private T[] items = new T[DefaultCapacity];

    public int Count { get; private set; } = 0;

    public T this[int index]
    {
        get { return items[index]; }
        set { items[index] = value; }
    }

    public void Add(T item)
    {
        if (Count >= items.Length)
            Array.Resize(ref items, items.Length * 2);

        items[Count] = item;
        Count++;    //  Count = Count + 1
    }
}

