using Core;
using System.Collections.Concurrent;

namespace ThreadingSamples;

class Samples : SamplesBase
{
    private int counter = 0;

    protected override void RunSamples()
    {
        // ShowBasics();

        ShowListSyncProblems();

        // ShowConcurrentCollections();

        // ShowMutex();
    }

    private void ShowBasics()
    {
        Thread currentThread = Thread.CurrentThread;

        Thread thread = new Thread(ThreadMain);
        int threadId = thread.ManagedThreadId;
        thread.Start(7);
        //  thread.Abort(); - не используйте

        Console.WriteLine("Before Join");
        thread.Join(TimeSpan.FromSeconds(10));
        Console.WriteLine("After Join");


        //  background или foreground
        // thread.IsBackground = false;

        var longBackgroundThread = new Thread(LongBackgroundThreadMain);
        longBackgroundThread.IsBackground = true;
        longBackgroundThread.Name = "Long Backgound";

        var shortForegroundThread = new Thread(ShortForegroundThreadMain);
        shortForegroundThread.Name = "Short Foreground";

        longBackgroundThread.Start();
        shortForegroundThread.Start();

        counter++;

        int someValue = 3;

        var thread2 = new Thread(o =>
        {
            Console.WriteLine(someValue);
        });
    }

    private void ThreadMain(object obj)
    {
        Console.WriteLine("Hello from other thread: {0}", obj);

        Thread.Sleep(3000);

        Console.WriteLine("End of thread");
    }

    private void LongBackgroundThreadMain(object obj)
    {
        Console.WriteLine("Start of background thread");
        Thread.Sleep(10_000);
        counter++;
        Console.WriteLine("End of background thread");
    }

    private void ShortForegroundThreadMain(object obj)
    {
        Console.WriteLine("Start of foreground thread");
        Thread.Sleep(5_000);
        counter++;
        Console.WriteLine("End of foreground thread");
    }

    private void ShowListSyncProblems()
    {
        var list = new MyList<int>();
        // var list = new MyListWithLock<int>();
        var random = new Random();

        var threads = Enumerable.Range(0, 100)
            .Select(_ => new Thread(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    try
                    {
                        list.Add(random.Next(0, 1001));
                    }
                    catch
                    {

                    }
                }
            }))
            .ToArray();

        foreach (var thread in threads)
        {
            thread.Start();
        }

        foreach (var thread in threads)
        {
            thread.Join();
        }

        Console.WriteLine("List items count: {0}", list.Count);
    }

    private void ShowConcurrentCollections()
    {
        var bag = new ConcurrentBag<int>();

        var stack = new ConcurrentStack<int>();

        var queue = new ConcurrentQueue<int>();

        var dict = new ConcurrentDictionary<string, string>();
    }

    private void SHowInterlocked()
    {
        int i = 5;

        Interlocked.Add(ref i, 5);
    }

    private void ShowMutex()
    {
        var mutex = new Mutex();

        mutex.WaitOne();
        bool success = mutex.WaitOne(TimeSpan.FromSeconds(30));

        mutex.ReleaseMutex();

        //  системный мьютекс
        var systemMutex = new Mutex(false, "mutex name");

        //  single application pattern
        var singleAppGuard = new Mutex(false, "Моё приложение нельзя запускать дважды!");

        bool isAlreadyStarted = !singleAppGuard.WaitOne(TimeSpan.FromMilliseconds(1));

        if (isAlreadyStarted)
        {
            //  Показать пользователю сообщение, что так нельзя
            //  Завершить приложение
        }
    }

    private void ShowSemaphore()
    {
        var semaphore = new Semaphore(0, 3);

        semaphore.WaitOne();

        semaphore.Release();
    }

    private void ShowManualResetEvent()
    {
        var manualResetEvent = new ManualResetEvent(false);

        Console.WriteLine("Запускаю второй поток...");
        var thread = new Thread(_ =>
        {
            Console.WriteLine("Привет, мир!");
            manualResetEvent.Set();
        });
        thread.Start();

        manualResetEvent.WaitOne();
        manualResetEvent.Reset();
        Console.WriteLine("Второй поток запущен");

        //  ====================================================

        int a = 0;
        int b = 0;
        int sum = 0;
        var calcStartEvent = new ManualResetEvent(false);
        var calcFinishedEvent = new ManualResetEvent(false);

        var calcThread = new Thread(() =>
        {
            calcStartEvent.WaitOne();
            sum = a + b;
            calcFinishedEvent.Set();
        });
        calcThread.Start();

        Console.WriteLine("Введите 2 целых числа");
        a = int.Parse(Console.ReadLine());
        b = int.Parse(Console.ReadLine());
        calcStartEvent.Set();
        calcFinishedEvent.WaitOne();

        Console.WriteLine("Результат: {0}", sum);
    }

    private void ShowAutoResetEvent()
    {
        var autoResetEvent = new AutoResetEvent(false);
        var queue = new ConcurrentQueue<WorkerTask<int>>();
        bool shouldStop = false;
        var workerThreads = Enumerable.Range(0, 10)
            .Select(_ => new Thread(() =>
            {
                WorkerTask<int> task;

                while (queue.TryDequeue(out task))
                    task.Run();

                while (!shouldStop)
                {
                    autoResetEvent.WaitOne();

                    if (queue.TryDequeue(out task))
                        task.Run();
                }
            }))
            .ToArray();

        foreach (var thread in workerThreads)
        {
            thread.Start();
        }

        //  создание задачи пользователем
        var task = new WorkerTask<int>(() => 3 + 7);
        task.Complete += result => Console.WriteLine(result);

        //  запуск выполнения задачи
        queue.Enqueue(task);
        autoResetEvent.Set();

        //  остановка рабочих потоков
        shouldStop = true;
        while (!autoResetEvent.WaitOne(TimeSpan.FromMilliseconds(1)))
            autoResetEvent.Set();
    }

    class WorkerTask<TResult>
    {
        private readonly Func<TResult> fn;

        public event Action<TResult> Complete;

        public WorkerTask(Func<TResult> fn)
        {
            this.fn = fn;
        }

        public void Run()
        {
            Complete?.Invoke(fn());
        }
    }
}

