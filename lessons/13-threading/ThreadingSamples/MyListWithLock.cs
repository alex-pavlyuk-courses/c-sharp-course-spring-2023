namespace ThreadingSamples;

class MyListWithLock<T>
{
    private const int DefaultCapacity = 4;

    private readonly object syncRoot = new object();

    private T[] items = new T[DefaultCapacity];

    public int Count { get; private set; } = 0;

    public T this[int index]
    {
        get { return items[index]; }
        set { items[index] = value; }
    }

    public void Add(T item)
    {
        /*
        Monitor.Enter(syncRoot);
        Monitor.Exit(syncRoot);
        */

        lock (syncRoot)
        {
            if (Count >= items.Length)
                Resize(2);

            items[Count] = item;
            Count++;    //  Count = Count + 1
        }
    }

    public void RemoveLast()
    {
        lock (syncRoot)
        {
            Count--;
        }
    }

    private void Resize(int quantifier)
    {
        int newSize = quantifier * items.Length;
        T[] newItems = new T[newSize];

        items.CopyTo(newItems, 0);

        this.items = newItems;
    }
}

