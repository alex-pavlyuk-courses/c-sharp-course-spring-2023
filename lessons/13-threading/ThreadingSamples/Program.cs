﻿using ThreadingSamples;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

new Samples().Run();

/*

Процесс - экземпляр выполняющейся программы:
- образ исполняемого кода
- настройки безопасности
- стек
- куча
- файловые дескрипторы и другие ресурсы, которыми владеет процесс
- и т.д.

Изоляция между процессами поддерживается ОС. Кроме shared memory.

Временное мультиплексирование

*/