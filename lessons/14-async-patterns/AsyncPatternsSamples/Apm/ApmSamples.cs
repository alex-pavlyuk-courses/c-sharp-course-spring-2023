using Core;

namespace AsyncPatternsSamples.Apm;

/*

R DoWork(A a, B b)
{
    //  подготовка

    //  работа

    //  сбор результатов
}

IAsyncResult BeginDoWork(A a, B b, AsyncCallback cb, object obj)
{
    //  подготовка

    //  запуск работы в другом потоке
}

R EndDoWork(IAsyncResult ar)
{
    //  сбор результатов
}

*/

/// <summary>
/// Asynchronous programming model
/// </summary>
class ApmSamples : SamplesBase
{
    protected override void RunSamples()
    {
        Func<int> fn = () => 7;
        fn.Invoke();

        var asyncResult = fn.BeginInvoke(null, null);
        int result = fn.EndInvoke(asyncResult);

        using (var stream = File.OpenRead("file.txt"))
        {
            //  stream.ReadAsync
        }

    }

    private void ShowBlockingOnWaitHandle()
    {
        var service = new SearchService();

        var asyncResult = service.BeginSearch("cute dogs", null, null);

        asyncResult.AsyncWaitHandle.WaitOne();

        var result = service.EndSearch(asyncResult);
    }

    private void ShowBlockingOnEnd()
    {
        var service = new SearchService();

        var asyncResult = service.BeginSearch("cute dogs", null, null);

        var result = service.EndSearch(asyncResult);
    }

    private void ShowPollingForResult()
    {
        var service = new SearchService();

        var asyncResult = service.BeginSearch("cute dogs", null, null);

        while (!asyncResult.IsCompleted)
        {
            //  для примера, это не лучшее решение
            Thread.Sleep(100);
        }

        var result = service.EndSearch(asyncResult);
    }

    private void ShowCallback()
    {
        var service = new SearchService();

        service.BeginSearch(
            "cute dogs",
            asyncResult =>
            {
                var result = service.EndSearch(asyncResult);
            },
            null);


        service.BeginSearch(
            "cute dogs",
            OnSearchComplete,
            service);

    }

    private void OnSearchComplete(IAsyncResult asyncResult)
    {
        var service = asyncResult.AsyncState as SearchService;
        var result = service.EndSearch(asyncResult);
    }

    private void ShowDelegateBeginInvoke()
    {
        var service = new SearchService();

        Func<string, string[]> fn = q => service.Search(q);

        fn.BeginInvoke(
            "some cats",
            ar =>
            {
                fn.EndInvoke(ar);
            },
            null
        );
    }
}

