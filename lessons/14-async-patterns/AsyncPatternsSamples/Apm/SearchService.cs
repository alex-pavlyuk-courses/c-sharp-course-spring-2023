namespace AsyncPatternsSamples.Apm;

class SearchService
{
    public string[] Search(string query)
    {
        if (string.IsNullOrWhiteSpace(query))
            throw new ArgumentException("Query should be defined");

        //  Представьте, что эта задержка - ожидание ответа внешней системы
        Thread.Sleep(TimeSpan.FromSeconds(5));

        var result = new[] { "foo", "bar" };

        return result;
    }

    public IAsyncResult BeginSearch(string query, AsyncCallback callback, object state)
    {
        if (string.IsNullOrWhiteSpace(query))
            throw new ArgumentException("Query should be defined");

        var asyncResult = new AsyncResult(state);

        ThreadPool.QueueUserWorkItem(_ =>
        {
            try
            {
                Thread.Sleep(TimeSpan.FromSeconds(5));

                var result = new[] { "foo", "bar" };

                asyncResult.MarkAsCompleted(result);
            }
            catch (Exception ex)
            {
                asyncResult.MarkAsFailed(ex);
            }

            callback?.Invoke(asyncResult);
        });

        return asyncResult;
    }

    public string[] EndSearch(IAsyncResult asyncResult)
    {
        if (asyncResult == null)
            throw new ArgumentNullException();

        if (!(asyncResult is AsyncResult))
            throw new InvalidOperationException("Incorrect async result was provided");

        var concrete = asyncResult as AsyncResult;

        concrete.AsyncWaitHandle.WaitOne();

        if (concrete.Exception != null)
            throw concrete.Exception;

        return concrete.Result as string[];
    }

    private class AsyncResult : IAsyncResult
    {

        private readonly ManualResetEvent waitHandle = new ManualResetEvent(false);

        public Exception Exception { get; private set; }

        public object Result { get; private set; }

        #region IAsyncResult

        public object AsyncState { get; }

        public WaitHandle AsyncWaitHandle { get { return waitHandle; } }

        public bool CompletedSynchronously { get; } = false;

        public bool IsCompleted { get; private set; }

        #endregion

        public AsyncResult(object state)
        {
            AsyncState = state;
        }

        public void MarkAsCompleted(object result)
        {
            Result = result;
            IsCompleted = true;
            waitHandle.Set();
        }

        public void MarkAsFailed(Exception exception)
        {
            Exception = exception;
            IsCompleted = true;
            waitHandle.Set();
        }
    }
}

