using System.ComponentModel;

namespace AsyncPatternsSamples.Eap;

public class AsyncCompletedEventArgs<T> : AsyncCompletedEventArgs
{
    public T Result { get; }

    public AsyncCompletedEventArgs(Exception error, T result, bool cancelled, object userState)
        : base(error, cancelled, userState)
    {
        Result = result;
    }
}

