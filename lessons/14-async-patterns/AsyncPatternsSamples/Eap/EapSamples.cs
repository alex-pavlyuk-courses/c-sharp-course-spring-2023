using System.ComponentModel;
using Core;

namespace AsyncPatternsSamples.Eap;

class EapSamples : SamplesBase
{
    protected override void RunSamples()
    {
        //  Event-based asynchronous programming

        int requestId = 0;

        var service = new AsyncExample();

        service.Method1Completed += (_, e) =>
        {
            int currentRequestId = (int)e.UserState;

            if (currentRequestId == 1)
            {
                //  ...
            }
        };

        service.Method1Async("foo  bar", Interlocked.Increment(ref requestId));
        service.Method1Async("bar baz", Interlocked.Increment(ref requestId));
        service.Method1Async("hot fuzz", Interlocked.Increment(ref requestId));

        service.CancelAsync(42);
    }

    private void ShowBackgroundWorker()
    {
        var worker = new BackgroundWorker();

        worker.DoWork += (_, e) =>
        {
            if (e.Cancel)
                return;

            //  some calculations
            Thread.Sleep(TimeSpan.FromSeconds(3));

            worker.ReportProgress((int)(3.0 / 8 * 100));

            if (e.Cancel)
                return;

            //  some more calculations
            Thread.Sleep(TimeSpan.FromSeconds(5));

            worker.ReportProgress(100);

            //  Здесь обрабатывать отмену уже не будем, потому что результат готов

            int result = 42;

            e.Result = result;
        };

        worker.ProgressChanged += (_, e) =>
        {
            int percentage = e.ProgressPercentage;

            //  ...
        };

        worker.RunWorkerCompleted += (_, e) =>
        {
            if (e.Error != null)
            {
                // ...
            }
            else if (e.Cancelled)
            {
                //  ...
            }
            else
            {
                var result = e.Result;
            }
        };

        // когда пользователь запускает операцию
        worker.RunWorkerAsync();

        // когда пользователь хочет отменить
        worker.CancelAsync();
    }

    public class AsyncExample
    {
        private readonly HashSet<object> canceledIds = new HashSet<object>();

        private int inProgressCount = 0;

        public int Method1(string param)
        {
            //  some calculations
            Thread.Sleep(TimeSpan.FromSeconds(3));

            //  some more calculations
            Thread.Sleep(TimeSpan.FromSeconds(5));

            return 42;
        }

        public void Method1Async(string param, object userState)
        {
            Interlocked.Increment(ref inProgressCount);

            ThreadPool.QueueUserWorkItem(_ =>
            {
                AsyncCompletedEventArgs<int> args;
                try
                {
                    if (RaiseCanceledIfRequested(userState))
                        return;

                    //  some calculations
                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    if (RaiseCanceledIfRequested(userState))
                        return;

                    //  some more calculations
                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    //  Здесь обрабатывать отмену уже не будем, потому что результат готов

                    int result = 42;

                    args = new AsyncCompletedEventArgs<int>(null, result, false, userState);
                }
                catch (Exception ex)
                {
                    args = new AsyncCompletedEventArgs<int>(ex, default, false, userState);
                }

                Interlocked.Decrement(ref inProgressCount);
                Method1Completed?.Invoke(this, args);
            });
        }

        public event AsyncCompletedEventHandler<int> Method1Completed;

        public void CancelAsync(object userState)
        {
            canceledIds.Add(userState);
        }

        public bool IsBusy { get { return inProgressCount == 0; } }

        public event ProgressChangedEventHandler ProgressChanged;


        private bool RaiseCanceledIfRequested(object userState)
        {
            bool canceled = canceledIds.Contains(userState);

            if (canceled)
            {
                canceledIds.Remove(userState);
                var args = new AsyncCompletedEventArgs<int>(null, default, true, userState);
                Interlocked.Decrement(ref inProgressCount);
                Method1Completed?.Invoke(this, args);
            }

            return canceled;
        }

    }
}

