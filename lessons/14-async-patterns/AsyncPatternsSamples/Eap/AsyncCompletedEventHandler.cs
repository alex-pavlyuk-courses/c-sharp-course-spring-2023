namespace AsyncPatternsSamples.Eap;

public delegate void AsyncCompletedEventHandler<T>(object sender, AsyncCompletedEventArgs<T> e);
