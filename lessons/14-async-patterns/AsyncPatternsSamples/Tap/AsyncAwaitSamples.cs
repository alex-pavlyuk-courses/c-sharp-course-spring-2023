using Core;

namespace AsyncPatternsSamples.Tap;

class AsyncAwaitSamples : SamplesBase
{
    protected override void RunSamples()
    {
        throw new NotImplementedException();
    }

    async Task ShowWeatherInfoAsync2()
    {
        string location = await PromptLocationAsync();

        WeatherInfo info;

        try
        {
            info = await RequestWeatherInfoAsync(location);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Failed to resuest weather info");
            Console.WriteLine(ex);

            return;
        }

        await SaveWeatherInfoAsync(info);
    }

    Task ShowWeatherInfoAsync1()
    {
        return PromptLocationAsync()
            .ContinueWith(t => RequestWeatherInfoAsync(t.Result))
            .Unwrap()
            .ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Console.WriteLine("Failed to resuest weather info");
                    Console.WriteLine(t.Exception.InnerExceptions[0]);
                    return Task.CompletedTask;
                }
                else
                {
                    return SaveWeatherInfoAsync(t.Result);
                }
            })
            .Unwrap();
    }

    Task SaveWeatherInfoAsync(WeatherInfo info)
    {
        return Task.Delay(TimeSpan.FromSeconds(1));
    }

    Task<WeatherInfo> RequestWeatherInfoAsync(string region)
    {
        return Task.Delay(TimeSpan.FromSeconds(2))
            .ContinueWith(_ => new WeatherInfo());
    }

    Task<string> PromptLocationAsync()
    {
        return Task.Factory.StartNew(() =>
        {
            Console.WriteLine("Enter region");

            return Console.ReadLine();
        });
    }

    class WeatherInfo
    {

    }
}


