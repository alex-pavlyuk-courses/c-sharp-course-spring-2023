using Core;

namespace AsyncPatternsSamples.Tap;

class TapSamples : SamplesBase
{
    protected override void RunSamples()
    {
        //  Task-based asynchronous pattern
    }



    private void ShowTasks()
    {
        Action longRunningAction = () =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
        };
        Func<int> longRunningFunc = () =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
            return 42;
        };

        Task taskWithoutResult = new Task(longRunningAction);
        Task<int> taskWithResult = new Task<int>(longRunningFunc);

        taskWithoutResult.Start();  //  запустил задачу
        taskWithResult.Start();  //  запустил задачу

        //  ==================================================================================

        TaskScheduler scheduler = TaskScheduler.Default;    //  планировщик выполнения задач

        var taskFactory = new TaskFactory(scheduler);

        Task taskWithoutResult2 = taskFactory.StartNew(longRunningAction);  //  создал и запустил задачу с помощью указанного мной планировщика

        //  ==================================================================================

        var res = taskWithResult.Result;  //  ожидание завершения

        taskWithoutResult.RunSynchronously();
    }

    private void ShowWaitForResult()
    {
        Func<int> longRunningFunc = () =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
            return 42;
        };

        var task = new Task<int>(longRunningFunc);

        task.Start();

        if (task.Wait(TimeSpan.FromSeconds(10)))
        {
            ProcessTaskComplete(task);
        }
        else
        {
            Console.WriteLine("Не дождались, истёк таймаут");
        }
    }

    private void ShowContinuation()
    {
        Func<int> longRunningFunc = () =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
            return 42;
        };

        var task = new Task<int>(longRunningFunc);

        task.ContinueWith(ProcessTaskComplete);

        task.Start();
    }

    private void ShowContinuationChain()
    {
        var task = Task.Factory.StartNew(() =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
            return 42;
        }).ContinueWith(task =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(4));
            return task.Result * 17;
        }).ContinueWith(task =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(2));
            return $"final result = {task.Result}";
        }).ContinueWith(task =>
        {
            Console.WriteLine("Task result: {0}", task.Result);
        });

        Console.WriteLine("Task chain started");

        task.Wait();
    }

    private void ShowContinuationChainErrorOptions()
    {
        Task.Factory.StartNew(() =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(13));
            return 42;
        })
            .ContinueWith(task =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(4));
                return task.Result * 17;
            }, TaskContinuationOptions.OnlyOnRanToCompletion)
            .ContinueWith(task =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(2));
                return $"final result = {task.Result}";
            }, TaskContinuationOptions.OnlyOnRanToCompletion)
            .ContinueWith(task =>
            {
                Console.WriteLine("Task result: {0}", task.Result);
            }, TaskContinuationOptions.OnlyOnRanToCompletion)
            .ContinueWith(task =>
            {
                Console.WriteLine("Error: {0}", task.Exception);
            }, TaskContinuationOptions.OnlyOnFaulted)
            .ContinueWith(task =>
            {
                Console.WriteLine("Task was canceled");
            }, TaskContinuationOptions.OnlyOnCanceled);
    }

    private void ShowTaskAggregation()
    {
        var task1 = Task.Factory.StartNew(() =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(4));
            return 124;
        });
        var task2 = Task.Factory.StartNew(() =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(3));
            return "foo bar";
        });
        var task3 = Task.Factory.StartNew(() =>
        {
            Thread.Sleep(TimeSpan.FromSeconds(7));
            return false;
        });
        var allTasks = new Task[]
        {
                task1, task2, task3
        };

        Task.WhenAll(allTasks).ContinueWith(_ =>
        {
            Console.WriteLine("All tasks complete");
        });

        Task.WhenAny(allTasks).ContinueWith(_ =>
        {
            Console.WriteLine("One task complete");
        });

        Task.Factory.StartNew(() => 42)
            .ContinueWith(t => Task.WhenAll(new Task<int>[]
            {
                    Task.Factory.StartNew(() => t.Result * 4),
                    Task.Factory.StartNew(() => t.Result * 6),
                    Task.Factory.StartNew(() => t.Result * 7),
            }))
            .Unwrap()
            .ContinueWith(task =>
            {
                int[] results = task.Result;
            });
    }

    private void ShowTaskCancellation()
    {
        var cancellationTokenSource = new CancellationTokenSource();

        Task.Factory.StartNew(() =>
        {
            cancellationTokenSource.Token.ThrowIfCancellationRequested();

            //  some calculations
            Thread.Sleep(TimeSpan.FromSeconds(3));

            cancellationTokenSource.Token.ThrowIfCancellationRequested();

            //  some calculations
            Thread.Sleep(TimeSpan.FromSeconds(5));

            cancellationTokenSource.Token.ThrowIfCancellationRequested();

            //  some calculations
            Thread.Sleep(TimeSpan.FromSeconds(7));

            return 7;
        }, cancellationTokenSource.Token);

        //  когда пользователь запросил отмену.
        cancellationTokenSource.Cancel();
    }

    private void ProcessTaskComplete(Task<int> task)
    {
        if (task.IsFaulted)
        {
            AggregateException ex = task.Exception;
            Console.WriteLine("Errors occured:");

            foreach (var exception in ex.InnerExceptions)
            {
                Console.WriteLine(exception);
            }
        }
        else if (task.IsCanceled)
        {
            Console.WriteLine("Task was canceled");
        }
        else
        {
            Console.WriteLine("Task result: {0}", task.Result);
        }
    }

    private void ShowTaskComposition()
    {
        /**
         * root
         *   - child 1
         *      - child 1 of child 1
         *   - child 2
         *      - child 1 of child 2
         *      - child 2 of child 2
         *      - child 3 of child 2
         * 
         */
        var root = new Task<int>(() =>
        {
            Console.WriteLine("Root task started");

            var child1 = new Task(() =>
            {
                Console.WriteLine("Child 1 task started");

                var child1OfChild1 = new Task(() =>
                {
                    Console.WriteLine("Child 1 of child 1 task started");
                    Console.WriteLine("Child 1 of child 1 task completed");
                }, TaskCreationOptions.AttachedToParent);

                child1OfChild1.Start();

                Console.WriteLine("Child 1 task completed");
            }, TaskCreationOptions.AttachedToParent);

            var child2 = new Task(() =>
            {
                Console.WriteLine("Child 2 task started");

                var child1OfChild2 = new Task(() =>
                {
                    Console.WriteLine("Child 1 of child 2 task started");
                    Console.WriteLine("Child 1 of child 2 task completed");
                }, TaskCreationOptions.AttachedToParent);

                var child2OfChild2 = new Task(() =>
                {
                    Console.WriteLine("Child 2 of child 2 task started");
                    Console.WriteLine("Child 2 of child 2 task completed");
                }, TaskCreationOptions.AttachedToParent);

                var child3OfChild2 = new Task(() =>
                {
                    Console.WriteLine("Child 3 of child 2 task started");
                    Console.WriteLine("Child 3 of child 2 task completed");
                }, TaskCreationOptions.AttachedToParent);

                child1OfChild2.Start();
                child2OfChild2.Start();
                child3OfChild2.Start();

                Console.WriteLine("Child 2 task completed");
            }, TaskCreationOptions.AttachedToParent);

            child1.Start();
            child2.Start();

            Console.WriteLine("Root task completed");

            return 13;
        });
    }
}

