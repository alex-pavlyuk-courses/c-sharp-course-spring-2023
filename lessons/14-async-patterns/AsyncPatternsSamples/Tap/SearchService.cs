namespace AsyncPatternsSamples.Tap;

class SearchService
{
    public string[] Search(string query)
    {
        if (string.IsNullOrWhiteSpace(query))
            throw new ArgumentException("Query should be defined");

        Thread.Sleep(TimeSpan.FromSeconds(5));

        var result = new[] { "foo", "bar" };

        return result;
    }

    public Task<string[]> SearchAsync(string query)
    {
        return SearchAsync(query, null);
    }

    public Task<string[]> SearchAsync(string query, CancellationToken? token)
    {
        if (string.IsNullOrWhiteSpace(query))
            throw new ArgumentException("Query should be defined");

        return Task.Factory.StartNew(() =>
        {
            token?.ThrowIfCancellationRequested();

            Thread.Sleep(TimeSpan.FromSeconds(5));

            token?.ThrowIfCancellationRequested();

            Thread.Sleep(TimeSpan.FromSeconds(3));

            var result = new[] { "foo", "bar" };

            return result;
        });
    }
}

