﻿
PrintWelcome();

double x1 = PromptDouble("x1");

double x2 = PromptDouble("x2");

double eps = PromptDouble("eps");

double x0 = FindEquationSolution(x1, x2, eps);

PrintResult(x1, x2, eps, x0);

void PrintWelcome()
{
    Console.WriteLine("Welcome to Equation Solution Finder");
}

double PromptDouble(string name)
{
    string str;
    double result;
    bool success;

    do
    {
        Console.Write("Enter {0}> ", name);
        str = Console.ReadLine();
        success = double.TryParse(str, out result);

        if (!success)
            Console.WriteLine("I was unable to recognize \"{0}\" as a number. Try again please", str);

    } while (!success);

    return result;
}

double FindEquationSolution(double x1, double x2, double eps)
{
    double curEps;

    do
    {
        double xmid = (x1 + x2) / 2;

        if (f(x1) * f(xmid) < 0)
            x2 = xmid;
        else
            x1 = xmid;

        curEps = Math.Abs(x2 - x1) / 2;

    } while (curEps > eps);

    return (x1 + x2) / 2;
}

void PrintResult(double x1, double x2, double eps, double x0)
{
    Console.WriteLine("Result is {0}", x0);
}

double f(double x)
{
    return Math.Sin(x); //3.23 * x * x - 6.84 * x + 17.8;
}