﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Enter operand");

string opStr1 = Console.ReadLine();
double op1 = double.Parse(opStr1);

Console.WriteLine("Enter operator");
string op = Console.ReadLine();

Console.WriteLine("Enter operand");
string opStr2 = Console.ReadLine();
double op2 = double.Parse(opStr2);

double result = op1;

if (op == "+") 
{
    result = result + op2;
}
else if (op == "-")
{
    result -= op2;
}
else if (op == "*")
{
    result *= op2;
}
else if (op == "/")
{
    result /= op2;
}

Console.WriteLine("{0} {1} {2} = {3}", op1, op, op2, result);
