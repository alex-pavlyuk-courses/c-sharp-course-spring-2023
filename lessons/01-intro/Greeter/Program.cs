﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello! What's your name?");

string name = Console.ReadLine();

Console.WriteLine("Hello, " + name);

Console.WriteLine("Hello, {0}", name);

Console.WriteLine(string.Format("Hello, {0}", name));

Console.WriteLine($"Hello, {name}");
