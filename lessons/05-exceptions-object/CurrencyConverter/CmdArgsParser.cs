
public class CmdArgsParser
{
    private readonly IConsoleWrapper consoleWrapper;

    public CmdArgsParser(IConsoleWrapper consoleWrapper)
    {
        this.consoleWrapper = consoleWrapper;
    }

    public string GetArgument(string[] args, string? name = null)
    {
        string? result = null;

        if (name != null)
        {
            result = FindNamedArgument(args, name);
        }
        else
        {
            result = FindUnnamedArgument(args);
        }

        if (result == null)
        {
            consoleWrapper.WriteLine("Specify value for {0}>", name == null ? "amount" : name);
            result = consoleWrapper.ReadLine();
        }

        return result!;
    }

    private string? FindNamedArgument(string[] args, string name)
    {
        string? result = null;
        string prefix = $"--{name}=";

        for (int i = 0; i < args.Length && result == null; i++)
        {
            if (args[i].StartsWith(prefix))
            {
                result = args[i].Substring(prefix.Length);
            }
        }

        return result;
    }

    private string? FindUnnamedArgument(string[] args)
    {
        string? result = null;

        for (int i = 0; i < args.Length && result == null; i++)
        {
            if (!args[i].StartsWith("--"))
            {
                result = args[i];
            }
        }

        return result;
    }
}