
using CurrencyConverter;

public class Converter 
{
    private readonly IExchangeRatesProvider exchangeRatesProvider;

    public Converter(IExchangeRatesProvider exchangeRatesProvider)
    {
        this.exchangeRatesProvider = exchangeRatesProvider;
    }

    public decimal Exchange(Currency from, Currency to, decimal value)
    {
        var currencyRatesToRub = exchangeRatesProvider.ProvideCurrencyRatesToRub();

        return 7;
    }
}

