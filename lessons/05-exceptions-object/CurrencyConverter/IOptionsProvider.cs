namespace CurrencyConverter;

public interface IOptionsProvider
{
    Options Provide();
}
