namespace CurrencyConverter;

public interface IExchangeRatesProvider
{
    Dictionary<Currency, decimal> ProvideCurrencyRatesToRub();
}
