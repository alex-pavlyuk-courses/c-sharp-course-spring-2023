using CurrencyConverter;

public class CurrencyConverterService
{
    private readonly IConsoleWrapper consoleWrapper;
    private readonly IOptionsProvider optionsProvider;
    private readonly Converter converter;

    public CurrencyConverterService(IConsoleWrapper consoleWrapper, IOptionsProvider optionsProvider, Converter converter)
    {
        this.consoleWrapper = consoleWrapper;
        this.optionsProvider = optionsProvider;
        this.converter = converter;
    }

    public void Run()
    {
        var options = optionsProvider.Provide();

        decimal result = converter.Exchange(options.From, options.To, options.Amount);

        consoleWrapper.WriteLine("{0} {1} is {2} {3}", options.Amount, options.From, result, options.To);
    }
}