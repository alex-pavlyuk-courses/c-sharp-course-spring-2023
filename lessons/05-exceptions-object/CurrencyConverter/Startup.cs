using Microsoft.Extensions.DependencyInjection;

namespace CurrencyConverter;

public class Startup
{
    public IServiceProvider BuildServiceProvider(string[] cmdArgs)
    {
        return ConfigureServices(cmdArgs)
            .BuildServiceProvider();
    }

    protected IServiceCollection ConfigureServices(string[] cmdArgs)
    {
        return new ServiceCollection()
            .AddScoped<CurrencyConverterService>()
            .AddScoped<IConsoleWrapper, ConsoleWrapper>()
            .AddScoped<IOptionsProvider, ConsoleOptionsProvider>(_ => new ConsoleOptionsProvider(cmdArgs, _.GetService<CmdArgsParser>()!))
            .AddScoped<CmdArgsParser>()
            .AddScoped<IConsoleWrapper>()
            .AddScoped<Converter>()
            .AddScoped<IExchangeRatesProvider, HardCodedExchangeRatesProvider>();
    }
}
