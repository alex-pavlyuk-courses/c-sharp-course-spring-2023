namespace CurrencyConverter;

public class HardCodedExchangeRatesProvider : IExchangeRatesProvider
{
    public Dictionary<Currency, decimal> ProvideCurrencyRatesToRub()
    {
        return new Dictionary<Currency, decimal>()
        {
            {Currency.KZT, 0.17m},
            {Currency.USD, 77.01m},
            {Currency.EUR, 83.71m},
            {Currency.GBP, 94.69m},
        };
    }
}
