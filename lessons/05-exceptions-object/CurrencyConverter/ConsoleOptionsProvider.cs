namespace CurrencyConverter;

public class ConsoleOptionsProvider : IOptionsProvider
{
    private readonly string[] cmdArgs;
    private readonly CmdArgsParser cmdArgsParser;

    public ConsoleOptionsProvider(string[] cmdArgs, CmdArgsParser cmdArgsParser)
    {
        this.cmdArgs = cmdArgs;
        this.cmdArgsParser = cmdArgsParser;
    }

    public Options Provide()
    {
        string fromStr = cmdArgsParser.GetArgument(cmdArgs, "from");
        string toStr = cmdArgsParser.GetArgument(cmdArgs, "to");
        string amountStr = cmdArgsParser.GetArgument(cmdArgs);

        Currency from;

        if (!Enum.TryParse<Currency>(fromStr, out from))
        {
            throw new Exception($"Currency {fromStr} is not supported");
        }

        Currency to;


        if (!Enum.TryParse<Currency>(toStr, out to))
        {
            throw new Exception($"Currency {toStr} is not supported");
        }

        decimal amount;

        if (!decimal.TryParse(amountStr, out amount))
        {
            throw new Exception($"Failed to parse {amountStr} as decimal value");
        }

        return new Options(from, to, amount);
    }
}
