namespace CurrencyConverter;

public class Options
{
    public Currency From { get; }

    public Currency To { get; }

    public decimal Amount { get; }

    public Options(Currency from, Currency to, decimal amount)
    {
        From = from;
        To = to;
        Amount = amount;
    }
}
