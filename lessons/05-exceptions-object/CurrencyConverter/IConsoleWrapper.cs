public interface IConsoleWrapper
{
    string? ReadLine();

    void WriteLine(string formatString, params object[] args);
}