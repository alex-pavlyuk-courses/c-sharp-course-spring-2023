class ConsoleWrapper : IConsoleWrapper
{
    public string? ReadLine()
    {
        return Console.ReadLine();
    }

    public void WriteLine(string formatString, params object[] args)
    {
        Console.WriteLine(formatString, args);
    }
}
