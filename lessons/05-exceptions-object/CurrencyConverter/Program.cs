﻿using CurrencyConverter;
using Microsoft.Extensions.DependencyInjection;

new Startup()
    .BuildServiceProvider(args)
    .GetService<CurrencyConverterService>()
    !.Run();
