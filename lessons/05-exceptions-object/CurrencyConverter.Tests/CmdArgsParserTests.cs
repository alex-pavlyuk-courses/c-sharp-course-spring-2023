using CurrencyConverter.Tests.Mocks;

namespace CurrencyConverter.Tests;

public class CmdArgsParserTests
{
    [Theory(DisplayName = "Parse from argument")]
    [InlineData("--from=USD", "--to=GBP", "324.23")]
    [InlineData("--to=GBP", "--from=USD", "324.23")]
    [InlineData("324.23", "--from=USD", "--to=GBP")]
    public void ParseFromTests(params string[] args)
    {
        //  arrange
        var parser = new CmdArgsParser(new ConsoleWrapperMock());
        string expected = "USD";

        //  act
        string actual = parser.GetArgument(args, "from");

        //  assert
        Assert.Equal(expected, actual);
    }

    [Theory(DisplayName = "Parse to argument")]
    [InlineData("--from=USD", "--to=GBP", "324.23")]
    [InlineData("--to=GBP", "--from=USD", "324.23")]
    [InlineData("324.23", "--from=USD", "--to=GBP")]
    public void ParseToTests(params string[] args)
    {
        //  arrange
        var parser = new CmdArgsParser(new ConsoleWrapperMock());
        string expected = "GBP";

        //  act
        string actual = parser.GetArgument(args, "to");

        //  assert
        Assert.Equal(expected, actual);
    }

    [Theory(DisplayName = "Parse amont argument")]
    [InlineData("--from=USD", "--to=GBP", "324.23")]
    [InlineData("--to=GBP", "--from=USD", "324.23")]
    [InlineData("324.23", "--from=USD", "--to=GBP")]
    public void ParseAmountTests(params string[] args)
    {
        //  arrange
        var parser = new CmdArgsParser(new ConsoleWrapperMock());
        string expected = "324.23";

        //  act
        string actual = parser.GetArgument(args);

        //  assert
        Assert.Equal(expected, actual);
    }
}