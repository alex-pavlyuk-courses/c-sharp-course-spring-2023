using CurrencyConverter.Tests.Mocks;

namespace CurrencyConverter.Tests;

public class ConverterTests
{
    [Fact]
    public void ExchangeTest()
    {
        //  arrange
        var exchangeRatesProvider = new ExchangeRatesProviderMock();
        var converter = new Converter(exchangeRatesProvider);
        exchangeRatesProvider.CurrencyRatesToRub[Currency.GBP] = 94.69m;
        Currency from = Currency.GBP;
        Currency to = Currency.RUB;
        decimal expedted = 94.69m;

        //  act
        decimal actual = converter.Exchange(from, to, 1);

        //  assert
        Assert.Equal(expedted, actual);
    }
}