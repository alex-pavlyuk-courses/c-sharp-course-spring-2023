namespace CurrencyConverter.Tests.Mocks;

public class OptionsProviderMock : IOptionsProvider
{
    public Options? Options { get; set; }

    public Options Provide()
    {
        return Options!;
    }
}
