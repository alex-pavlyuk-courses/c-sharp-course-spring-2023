namespace CurrencyConverter.Tests.Mocks;

internal class ConsoleWrapperMock : IConsoleWrapper
{
    private string? nextLineToRead;

    public List<string> WrittenLines { get; } = new List<string>();

    public void SetNextLineToRead(string line)
    {
        this.nextLineToRead = line;
    }


    public string? ReadLine()
    {
        return nextLineToRead;
    }

    public void WriteLine(string formatString, params object[] args)
    {
        WrittenLines.Add(string.Format(formatString, args));
    }
}