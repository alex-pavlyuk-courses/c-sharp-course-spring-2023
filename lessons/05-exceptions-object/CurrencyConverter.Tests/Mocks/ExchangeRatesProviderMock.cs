namespace CurrencyConverter.Tests.Mocks;

public class ExchangeRatesProviderMock : IExchangeRatesProvider
{
    public Dictionary<Currency, decimal> CurrencyRatesToRub { get; set; } = new Dictionary<Currency, decimal>();

    public Dictionary<Currency, decimal> ProvideCurrencyRatesToRub()
    {
        return CurrencyRatesToRub;
    }
}
