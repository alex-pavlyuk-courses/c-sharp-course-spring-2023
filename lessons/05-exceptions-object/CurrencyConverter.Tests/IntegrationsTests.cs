using CurrencyConverter.Tests.Mocks;

namespace CurrencyConverter.Tests;

public class IntegrationTests
{
    [Fact]
    public void IntegrationTest()
    {
        //  arrange
        var consoleWrapper = new ConsoleWrapperMock();
        var optionsProvider = new OptionsProviderMock();
        var exchangeRatesProvider = new ExchangeRatesProviderMock();
        var converter = new Converter(exchangeRatesProvider);
        var service = new CurrencyConverterService(consoleWrapper, optionsProvider, converter);

        optionsProvider.Options = new Options(Currency.USD, Currency.GBP, 324.23m);
        exchangeRatesProvider.CurrencyRatesToRub[Currency.USD] = 80m;
        exchangeRatesProvider.CurrencyRatesToRub[Currency.GBP] = 121m;

        //  act
        service.Run();

        //  assert
        Assert.Equal("324.23 USD is 7 GBP", consoleWrapper.WrittenLines[0]);
    }
}
