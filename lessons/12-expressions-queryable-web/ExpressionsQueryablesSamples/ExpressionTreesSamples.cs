using System.Linq.Expressions;

namespace ExpressionsQueryablesSamples;

public class ExpressionTreesSamples
{
    public void Run()
    {
        Func<int, int, bool> lambda = (x, y) => x > 3 * y;
        Expression<Func<int, int, bool>> expression = (x, y) => x > 3 * y;

        ParameterExpression xParam = expression.Parameters[0];

        //  synthesis

        var aParam = Expression.Parameter(typeof(int), "a");
        var bParam = Expression.Parameter(typeof(int), "b");
        var multiply = Expression.Multiply(Expression.Constant(3, typeof(int)), bParam);
        var body = Expression.GreaterThan(aParam, multiply);
        var generated = Expression.Lambda<Func<int, int, bool>>(body, aParam, bParam);

        Func<int, int, bool> compiled = generated.Compile();

        bool result = compiled(3, 4);

    }
}
