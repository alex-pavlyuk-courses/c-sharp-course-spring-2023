using Microsoft.EntityFrameworkCore;

namespace ExpressionsQueryablesSamples;

//  ORM: object-relational mapping
public class BikeStoreDb : DbContext
{
    public DbSet<Customer>? Customers { get; set; }

    public DbSet<Order>? Orders { get; set; }

    public DbSet<Employee>? Employees { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);

        optionsBuilder
            .UseNpgsql("Host=localhost;Database=bike_store;Username=postgres;Password=postgres")
            .LogTo(Console.WriteLine);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder
            .Entity<Employee>()
            .HasOne(e => e.Manager)
            .WithMany(e => e.Employees)
            .HasForeignKey(e => e.ManagerId)
            .HasPrincipalKey(e => e.Id);

        modelBuilder
            .Entity<Employee>()
            .HasOne(e => e.Hr)
            .WithMany(e => e.HrEmployees)
            .HasForeignKey(e => e.HrId)
            .HasPrincipalKey(e => e.Id);
    }
}
