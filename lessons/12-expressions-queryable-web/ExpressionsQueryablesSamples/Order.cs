namespace ExpressionsQueryablesSamples;

public class Order
{
    public long Id { get; set; }

    public long CustomerId { get; set; }

    public virtual Customer? Customer { get; set; }
}
