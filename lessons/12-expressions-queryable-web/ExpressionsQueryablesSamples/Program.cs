﻿// See https://aka.ms/new-console-template for more information
using ExpressionsQueryablesSamples;
using Microsoft.EntityFrameworkCore;

Console.WriteLine("Hello, World!");

new ExpressionTreesSamples().Run();


//GenerateData();

//  ADO.NET


using (var db = new BikeStoreDb())
{
    var customers = db.Customers
        .Where(e => e.Fullname.Contains("Вас") && e.Fullname.Length > 3 && e.Id > 0)
        .OrderBy(e => e.Fullname)
        .ToArray();

    db.Orders.Add(new Order { CustomerId = customers[0].Id });
    db.Orders.Add(new Order { CustomerId = customers[0].Id });
    db.Orders.Add(new Order { CustomerId = customers[0].Id });

    db.SaveChanges();

    
}

void GenerateData()
{
    using (var db = new BikeStoreDb())
    {
        db.Customers.Add(new Customer { Fullname = "Пупкин Василий" });
        db.Customers.Add(new Customer { Fullname = "Васечкин Пётр" });
        db.Customers.Add(new Customer { Fullname = "Иванов Егор" });
        db.Customers.Add(new Customer { Fullname = "Егоров Дмитрий" });

        db.SaveChanges();
    }

    var p = new Point(4, 5, 6);

}


record Point(double X, double Y, double Z)
{
    public void DoWork()
    {
        
    }
}