namespace ExpressionsQueryablesSamples;

public class Employee
{
    public long Id { get; set; }

    public long? ManagerId { get; set; }

    public long? HrId { get; set; }

    public virtual Employee Manager { get; set; }

    public virtual Employee Hr { get; set; }

    public virtual IEnumerable<Employee> Employees { get; set; }

    public virtual IEnumerable<Employee> HrEmployees { get; set; }
}
