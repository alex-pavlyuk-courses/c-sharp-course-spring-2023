namespace ExpressionsQueryablesSamples;

class QueryableSamples 
{
    public void Run()
    {
        IQueryable<int> q = Enumerable
            .Empty<int>()
            .AsQueryable();

        var list = q
            .Select(i => i * 7)
            .Where(i => i < 16)
            .ToList();

        //  LINQ to objects
        //  LINQ to something: LINQ to SQL, LINQ to XML etc
        
    }
}