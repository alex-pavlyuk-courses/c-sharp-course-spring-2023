namespace ExpressionsQueryablesSamples
{
    public class Customer
    {
        public long Id { get; set; } 

        public string? Fullname { get; set; }

        public virtual IEnumerable<Order> Orders { get; set; }
    }
}