using System.Collections;

namespace EnumarableAndGenericsSamples;

public class Fibbonacci : IEnumerable<int>
{
    public IEnumerator<int> GetEnumerator()
    {
        return new FibbonacciEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    class FibbonacciEnumerator : IEnumerator<int>
    {
        private int previous;

        private int current;

        public FibbonacciEnumerator()
        {
            Reset();
        }

        public int Current
        {
            get
            {
                if (current == 0)
                {
                    throw new InvalidOperationException($"Enumeration is not started yet. Call {nameof(MoveNext)}() before accessing {nameof(Current)} property");
                }

                return current;
            }
            private set { current = value; }
        }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (previous <= 0)
            {
                previous++;
            }
            else
            {
                int next = previous + current;

                previous = current;
                current = next;
            }

            return true;
        }

        public void Reset()
        {
            previous = -1;
            current = 1;
        }
    }
}
