﻿
using EnumarableAndGenericsSamples;

IEnumerable<int> enumerable = new int[] { 1, 2, 3, 4, 5 };

var enumerator = enumerable.GetEnumerator();

while (enumerator.MoveNext())
{
    int current = enumerator.Current;

    Console.WriteLine(current);
}

Console.WriteLine("=============");


foreach (int current in enumerable)
{
    Console.WriteLine(current);
}

ICollection<int> collection;
IList<int> list = new List<int>();

Console.WriteLine("=============");

var fib = new Fibbonacci()
    .Take(10);

foreach (int e in fib)
{
    Console.WriteLine(e);
}


int[] arr = new int[] { 1, 2, 3, 4, 5, 5, 32, 1, 34, 53, 1, 53};

ArrayUtils.Sort(arr, (a, b) => a - b);

//  Covariance
IEnumerable<string> strings = new List<string>();
IEnumerable<object> objects = strings;
IEnumerable<string> strings2 = objects;

//  Contravariance
Action<object> actOnObject = o => {};
Action<string> actOnString = actOnObject;
Action<object> actOnObject2 = actOnString;

R Method<T, R>(T arg)
{
    throw new NotImplementedException();
}
