namespace EnumarableAndGenericsSamples;

public class MyList<T, U, R, TT, UU, RR>
    where T : class
    where U : struct
    where R : new()
    where TT: notnull
{
    private T[] items;


    void DoWork()
    {
        R r = new R();
        T t = default(T);
        U u = default(U);
    }

}

interface IFoo<out T>
{
    T DoWork()
    {
        throw new NotImplementedException();
    }
}

class ArrayUtils
{
    public static void Sort<T>(T[] items, Func<T, T, int> compare)
    {
        for (int i = 0; i < items.Length; i++)
            for (int j = i + 1; j < items.Length; j++)
                if (compare(items[i], items[j]) > 0)
                {
                    T temp = items[i];
                    items[i] = items[j];
                    items[j] = temp;
                }
    }

    public static void Sort<T>(T[] items)
        where T : IComparable<T>
    {
        Sort(items, (a, b) => a.CompareTo(b));
    }
}