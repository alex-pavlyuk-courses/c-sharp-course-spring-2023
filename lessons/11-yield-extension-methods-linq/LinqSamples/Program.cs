﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

var random = new Random();
var enumerable = Enumerable.Range(1, 5)
    .SelectMany(e => Enumerable.Range((int)Math.Pow(10, e), 10))
    .Select(e => e * random.NextDouble());

foreach (double i in enumerable)
{
    Console.WriteLine(i);
}

var first = Enumerable.Range(1, 10).Where(e => e % 2 == 0);
var second = Enumerable.Range(100, 10).Where(e => e % 2 == 0);

first.Zip(second, (a, b) => $"{a} - {b}").Print();


//  sorting
Enumerable
    .Repeat(10, 10)
    .Select(e => e * random.Next(1, 11))
    .Select(e => new Person {Surname = "", Age = e})
    .OrderBy(e => e.Surname).ThenByDescending(e => e.Age)
    .Reverse()
    .Print();

//  quantifiers
bool b = enumerable.All(e => e > 0);
b = enumerable.Any(e => e % 17 == 0);
b = enumerable.Contains(4.5);

//  elements
double d = enumerable.ElementAt(7);
d = enumerable.First();
double? d1 = enumerable.FirstOrDefault();
d = enumerable.Last();
d1 = enumerable.LastOrDefault();
d = enumerable.Single(e => e > 109);    //  если ни одного или больше одного, то исключение
d1 = enumerable.SingleOrDefault();      //  если ни одного - default(T), если больше одного, то исключение


//  paging
enumerable = enumerable.Skip(3).Take(5);

//  aggregation
int count = enumerable.Count();
Enumerable
    .Repeat(10, 10)
    .Select(e => e * random.Next(1, 11))
    .Select(e => new Person { Surname = "", Age = e })
    .Min(e => e.Age);
//  Max, Average, Sum

//  sets
enumerable.Union(Enumerable.Range(-10, 100))
    .Except(Enumerable.Range(-5, 15))
    .Intersect(Enumerable.Range(-50, 150));

var people = Enumerable
    .Repeat(10, 10)
    .Select(e => e * random.Next(1, 11))
    .Select(e => new Person { Surname = "", Age = e })
    .GroupBy(e => e.Surname);

foreach (var group in people)
{
    Console.WriteLine("Surname: " + group.Key);

    foreach (var p in group)
    {
        Console.WriteLine("{0} {1}", p.Surname, p.Age);
    }
}


var query =
    from e in enumerable
    where e > 15
    select e * random.Next();



static class EnumerableExtensions 
{
    public static IEnumerable<T> Print<T>(this IEnumerable<T> enumerable)
    {
        foreach (var e in enumerable)
        {
            Console.WriteLine(e);
        }

        return enumerable;
    }
}

class Person
{
    public string Surname { get; set; }

    public int Age { get; set; }
}