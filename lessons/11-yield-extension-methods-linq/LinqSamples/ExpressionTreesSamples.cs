using System.Linq.Expressions;

namespace LinqSamples;

public class ExpressionTreesSamples
{
    public void Run()
    {
        Func<int, int, bool> lambda = (x, y) => x > 3 * y;
        Expression<Func<int, int, bool>> expression = (x, y) => x > 3 * y;


    }
}
