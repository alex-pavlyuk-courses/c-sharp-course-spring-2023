using System.Reflection;

namespace LinqSamples;

public class ReflectionSamples
{
    public int SomeProp { get; set; } = 7;

    public void Run()
    {
        Type type = typeof(ReflectionSamples);
        type = this.GetType();


        var members = type.GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        object value = type.GetProperties()[0].GetMethod.Invoke(this, Array.Empty<object>());


        AppSettings appSettings = new IniFileParser().Parse<AppSettings>("path.ini");

    }


    class AppSettings
    {
        [IniFileProperty(NameInFile = "db-conn-teemeout")]
        public string DbConnectionTimeout { get; set; }

        public string DbUsername { get; set; }

        public string DbPassword { get; set; }

        public string DbHost { get; set; }

        public string DbPort { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    class IniFilePropertyAttribute : Attribute
    {
        public string NameInFile { get; set; }
    }

    /*
    Key=value
    Key2=value2
    */
    class IniFileParser
    {
        public T Parse<T>(string path)
            where T: new()
        {
            T result = new T();

            using (var stream = File.OpenRead(path))
            using (var reader = new StreamReader(stream))
            {
                string? line;

                while ((line = reader.ReadLine()) != null)
                {
                    var parts = line.Split('=');

                    AssignPropertyValue(result, parts[0], parts[1]);
                }
            }

            return result;
        }

        private void AssignPropertyValue<T>(T instance, string key, string value)
        {
            var prop = instance.GetType()
                .GetProperties()
                .FirstOrDefault(e => 
                {
                    var attr = e.GetCustomAttributes<IniFilePropertyAttribute>().FirstOrDefault();

                    return attr == null
                        ? e.Name == key
                        : attr.NameInFile == key;
                });

            if (prop != null)
            {
                prop.SetMethod.Invoke(instance, new object[] { Convert.ChangeType(value, prop.PropertyType) });
            }
        }
    }
}
