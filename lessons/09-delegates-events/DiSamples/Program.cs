﻿using Microsoft.Extensions.DependencyInjection;


IServiceCollection serviceCollection = new ServiceCollection()
    .AddScoped<IDataAccess, DatabaseDataAccess>()
    .AddSingleton<DatabaseConnectionOptions>(p => 
    {
        string? connectionString = Environment.GetEnvironmentVariable("CONNECTION_STR");

        if (connectionString == null)
        {
            Console.WriteLine("Connection string>");
            connectionString = Console.ReadLine();
        }

        Console.WriteLine(connectionString);

        return new DatabaseConnectionOptions
        {
            ConnectionString = connectionString!,
        };
    });

IServiceProvider serviceProvider =  serviceCollection.BuildServiceProvider();

IDataAccess? dataAccess = serviceProvider.GetService<IDataAccess>();
