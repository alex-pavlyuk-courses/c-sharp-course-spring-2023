interface IDataAccess
{
    Data LoadData();

    Data SaveData();
}
