class DatabaseConnectionOptions
{
    public string ConnectionString { get; set; }

    public int ConnectionTimeout { get; set; }

}