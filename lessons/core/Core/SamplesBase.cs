namespace Core;

public abstract class SamplesBase
{
    private int currentExampleIndex = 0;

    public void Run()
    {

        Console.WriteLine("================= {0} ================= ", this);

        RunSamples();

        Console.WriteLine("================= {0} ================= ", this);
        Console.WriteLine();
    }

    protected abstract void RunSamples();

    protected void WriteExampleStarted(string title)
    {
        currentExampleIndex++;

        Console.WriteLine("Пример {0}. {1}", currentExampleIndex, title);
    }

    protected void WriteExampleFinished()
    {
        Console.WriteLine("Конец примера {0}", currentExampleIndex);
        Console.WriteLine();
    }
}

